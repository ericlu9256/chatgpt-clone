const res = require("express/lib/response");
const express = require('express');
// Add bory parser and cors to express
const bodyParser = require('body-parser');
const cors = require ('cors');
const { Configuration, OpenAIApi } = require("openai"); 
const { response } = require("express");
// const APIKEY = process.env.OPENAI_API_KEY;
require('dotenv').config();
const configuration = new Configuration({
    organization: "org-CfF0Y2HLyPbtC394zLVSxKPN",
    apiKey: "",
});
const openai = new OpenAIApi(configuration);


// Create a simple express API that calls the function above
const app = express()
// add cors to express 
app.use(bodyParser.json())
app.use(cors())
const port = 3080

app.post(`/`, async(req, res) => {
    const {message , currentModel} = req.body;
    console.log(message, "message");
    console.log(currentModel, "currentModel");
  
    const response = await openai.createCompletion({
        model: `${currentModel}`,//"text-davinci-003",
        prompt: `${message}`,
        max_tokens: 100,
        temperature: 0.5,
      });
    res.json({
        message:response.data.choices[0].text
      })
});

app.get(`/models`, async(res) => 
        {
            const response = await openai.listEngines();
            // const response = await openai.listModels();
            console.log(response.data);    
            res.json({
                models:response.data
            })
      });

app.listen(port,() => {
    console.log(`Example app listening at http://localhost:${port}`)
});
 